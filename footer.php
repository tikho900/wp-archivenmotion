<div class="footer container-fluid container-main">
    <div class="d-flex justify-content-between">
        <div class="footer-box">
            <div class="d-flex align-items-center gap-2">
                <img class="navbar-logo" src="https://archivemotion.com/wp-content/uploads/2024/06/logo-black.png" />
                <div class="footer-title-logo fw-bold ">ARCHIVE MOTION</div>
            </div>
            <div class="footer-info">개인정보보호책임자 : 서동찬 <a href="#" class="footer-link">(info@archivemotion.com)</a></div>
            <div class="footer-info">
                <div>+82 1577-6920</div>
                <div>archivemotion.com</div>
                <div>서울특별시 서초구 동광로1길 95, 201호 (방배동)</div>
            </div>
            <div class="footer-info">© 2024 ARCHIVE MOTION</div>
        </div>
        <a href="#" class="d-none d-lg-block align-content-center">
            <img class="object-fit-cover" src="<?php bloginfo('template_directory'); ?>/assets/images/talk.svg" />
        </a>
        <a href="#" class="footer-contact d-block d-lg-none">
            <img class="object-fit-cover" src="<?php bloginfo('template_directory'); ?>/assets/images/talk.svg" />
        </a>
    </div>
</div>

<?php wp_footer(); ?>

</body>

</html>