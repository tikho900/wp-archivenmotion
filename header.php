<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <?php
    if (function_exists('wp_body_open')) {
        wp_body_open();
    }
    ?>
    <!-- banner -->

    <div class="banner">

        <!-- navbar -->

        <header>
            <nav class="navbar navbar-expand-lg align-items-start py-2 py-lg-3">
                <div class="container-fluid navbar-box">
                    <a class="navbar-brand" href="/">
                        <div class="d-flex justify-content-center align-items-end header-logo">
                            <img class="navbar-logo"
                                src="https://archivemotion.com/wp-content/uploads/2024/06/logo.png" />
                        </div>
                    </a>

                    <div class="d-flex justify-content-center align-items-center gap-3">
                        <div class="mb-0 d-block d-lg-none">
                            <a class="header-btn" href="#">상담 신청</a>
                        </div>
                        <button class="navbar-toggler border-0 px-0" type="button" data-bs-toggle="offcanvas"
                            data-bs-target="#offcanvasExample" aria-controls="offcanvasExample" aria-expanded="false"
                            aria-label="Toggle navigation">
                            <span>
                                <svg width="32" height="32" viewBox="0 0 32 32" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path d="M5 16H27" stroke="white" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 8H27" stroke="white" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M5 24H27" stroke="white" stroke-width="2" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                </svg>
                            </span>
                        </button>
                    </div>

                    <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample"
                        aria-labelledby="offcanvasExampleLabel">
                        <div class="offcanvas-header">
                            <h5 class="offcanvas-title" id="offcanvasExampleLabel"></h5>
                            <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas"
                                aria-label="Close"></button>
                        </div>
                        <div class="offcanvas-body">
                            <div class="navbar-collapse pt-4 pt-lg-0" id="navbarSupportedContent">
                                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                                    <li
                                        class="nav-item nav-item-custom d-flex justify-content-lg-center align-items-center">
                                        <a class="nav-link nav-link-custom fw-normal" aria-current="page"
                                            href="#develop">TEXTILE
                                            R&amp;D</a>
                                    </li>
                                    <li
                                        class="nav-item nav-item-custom d-flex justify-content-lg-center align-items-center">
                                        <a class="nav-link nav-link-custom fw-normal" href="#interview">INTERVIEW</a>
                                    </li>
                                    <li
                                        class="nav-item nav-item-custom d-flex justify-content-lg-center align-items-center">
                                        <a class="nav-link nav-link-custom fw-normal" href="#clients">CLIENTS &amp;
                                            PARTNER</a>
                                    </li>
                                    <li
                                        class="nav-item nav-item-custom d-flex justify-content-lg-center align-items-center">
                                        <a class="nav-link nav-link-custom fw-normal" href="#faq">FAQ</a>
                                    </li>
                                    <li class="nav-item d-none d-lg-block">
                                        <a class="header-btn" href="#contact">상담 신청</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <!-- //navbar -->

        <!-- sologan -->

        <div class="container center">
            <div class="center-1">
                <span class="sologan-text-normal text-white">여러분의 </span>
                <span class="sologan-text-bold text-white">소재 데이터에</span>
            </div>
            <div class="center-2">
                <span class="sologan-text-normal text-white mt-2 mt-lg-0">아카이브모션이 <br class="d-block d-lg-none"> 함께
                    하겠습니다.</span>
            </div>
            <div class="center-3 d-flex justify-content-center mt-0 mt-lg-2">
                <div class="sologan-btn">
                    지금 바로 상담하기
                </div>
            </div>
        </div>

        <!-- //sologan -->

    </div>

    <!-- banner -->