<?php

/**
 * Define Constants
 */
define('ARCHI_THEME_DIR_URI', trailingslashit(get_template_directory_uri()));

if (!function_exists('archi_wp_enqueue_scripts')) {
    function archi_wp_enqueue_scripts()
    {
        wp_enqueue_style('style-main', ARCHI_THEME_DIR_URI . '/assets/css/styles.css?v=1.1.4');
        wp_enqueue_style('bootstrap', "https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css");
        wp_enqueue_style('splide', "https://cdn.jsdelivr.net/npm/@splidejs/splide@4.1.4/dist/css/splide.min.css");
        wp_enqueue_style('font-awesome', "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css");
        wp_enqueue_style('slick', "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css");
        wp_enqueue_style('slick-theme', "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css");

        wp_enqueue_script('bootstrap-js', "https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js");
        wp_enqueue_script('splide-js', "https://cdn.jsdelivr.net/npm/@splidejs/splide@4.1.4/dist/js/splide.min.js");
        wp_enqueue_script('slick', "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js", array('jquery'));
    }
    add_action('wp_enqueue_scripts', 'archi_wp_enqueue_scripts');
}
