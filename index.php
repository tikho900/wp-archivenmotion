<?php get_header(); ?>
<!-- container -->

<!-- sourcing-->

<div class="sourcing d-flex flex-column">
    <div class="container-fluid container-main">
        <div class="d-flex flex-column">
            <div class="sourcing-title mb-2">TEXTILE SOURCING</div>
            <div class="sourcing-title-sub">잘나가는 브랜드는</div>
            <div class="sourcing-title-sub"><span class="fw-bold">원단 소재</span>부터 다릅니다.</div>
            <div class="sourcing-title-des mt-3">브랜드 감성에 맞는 단독 개발된 원단 소재를 제안드리고 독특한 터치감을 제공합니다.</div>
        </div>

        <div class="row flex-column flex-lg-row pt-4 pt-lg-5 g-3 g-lg-5">
            <div class="col-12 col-xl-6 d-flex justify-content-between">
                <section id="splide-count" class="splide" aria-label="Splide Basic HTML Example">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <li class="splide__slide">
                                <div class="sourcing-card">
                                    <div class="d-flex flex-column justify-content-between h-100">
                                        <div>
                                            <div class="sourcing-splide-title">대한민국</div>
                                            <div class="sourcing-splide-sub">KOREA</div>
                                            <hr class="sourcing-splide-hr my-2" />
                                        </div>
                                        <div class="flag">
                                            <img
                                                src="<?php bloginfo('template_directory'); ?>/assets/images/korea.svg" />
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="splide__slide">
                                <div class="sourcing-card">
                                    <div class="d-flex flex-column justify-content-between h-100">
                                        <div>
                                            <div class="sourcing-splide-title">일본</div>
                                            <div class="sourcing-splide-sub">JAPAN</div>
                                            <hr class="sourcing-splide-hr my-2" />
                                        </div>
                                        <div class="flag">
                                            <img
                                                src="<?php bloginfo('template_directory'); ?>/assets/images/japan.svg" />
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="splide__slide">
                                <div class="sourcing-card">
                                    <div class="d-flex flex-column justify-content-between h-100">
                                        <div>
                                            <div class="sourcing-splide-title">이태리</div>
                                            <div class="sourcing-splide-sub">ITALY</div>
                                            <hr class="sourcing-splide-hr my-2" />
                                        </div>
                                        <div class="flag">
                                            <img
                                                src="<?php bloginfo('template_directory'); ?>/assets/images/italy.svg" />
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="splide__slide">
                                <div class="sourcing-card">
                                    <div class="d-flex flex-column justify-content-between h-100">
                                        <div>
                                            <div class="sourcing-splide-title">중국</div>
                                            <div class="sourcing-splide-sub">CHINA</div>
                                            <hr class="sourcing-splide-hr my-2" />
                                        </div>
                                        <div class="flag">
                                            <img
                                                src="<?php bloginfo('template_directory'); ?>/assets/images/china.svg" />
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
            <div class="col-12 col-xl-6 d-flex flex-column justify-content-center p-2">
                <div class="sourcing-des">
                    아카이브모션은 국내/해외 기본 원단부터 <br class="d-block d-lg-none"> 특수 원단까지 <br class="d-none d-lg-block">고객사의 다양한
                    니즈에 맞춰 <br class="d-block d-lg-none"> 품목별/가격대/품질별로 <br class="d-none d-lg-block">맞춤 소재를 제공합니다.
                </div>
            </div>
        </div>

        <div class="sourcing-count">
            <div class="d-flex justify-content-end sourcing-count-box">
                <div class="d-flex flex-column text-end">
                    <div class="sourcing-title-sub text-end">좋은 원단은</div>
                    <div class="sourcing-title-sub">
                        <span class="fw-bold">지속되는 리오더</span>
                        <span>로 <br class="d-block d-lg-none">증명됩니다</span>
                    </div>
                </div>
            </div>

            <div
                class="sourcing-counting row justify-content-between border-top border-bottom border-dark border-2 position-relative">
                <div class="col-12 col-lg-3 d-flex flex-column text-center">
                    <div class="sourcing-counting-title text-nowrap">누적 고객사</div>
                    <div class="d-flex justify-content-center align-items-center gap-2">
                        <div class="sourcing-counting-item counter" data-target="310"></div>
                        <div class="sourcing-counting-sub">brands</div>
                    </div>
                </div>
                <div class="col-12 col-lg-3 d-flex flex-column text-center">
                    <div class="sourcing-counting-title text-nowrap">평균 리오더 횟수</div>
                    <div class="d-flex justify-content-center align-items-center gap-2">
                        <div class="sourcing-counting-item counter" data-target="7"></div>
                        <div class="sourcing-counting-sub">th</div>
                    </div>
                </div>
                <div class="col-12 col-lg-3 d-flex flex-column text-center">
                    <div class="sourcing-counting-title text-nowrap">월 평균 납품 야드 수량</div>
                    <div class="d-flex justify-content-center align-items-center gap-2 pe-0 pe-lg-5">
                        <div class="sourcing-counting-item counter" data-target="120,000"></div>
                        <div class="sourcing-counting-sub">yd</div>
                    </div>
                </div>
                <div class="col-12 d-flex flex-column text-center d-block d-lg-none">
                    <div class="courcing-counting-year">2020~2024 기준</div>
                </div>
                <p class="position-absolute bottom-0 end-0 text-end d-none d-lg-block">2020~2024 기준</p>
            </div>
        </div>
    </div>
</div>

<!-- //sourcing -->

<!-- develop -->

<div id="develop" class="develop container-fluid text-white">
    <div class="row h-100">
        <div class="col-lg-6 pe-lg-4">
            <div class="d-flex flex-column justify-content-between h-100">
                <div>
                    <h4 class="develop-title-top">REASEARCH & DEVELOPMENT</h4>
                    <h4 class="develop-title-top">CENTER</h4>
                </div>
                <div class="develop-box d-flex flex-column">
                    <span class="develop-title-center mt-3 mt-lg-0">TEXTILE</span>
                    <span class="develop-title-center">R&D</span>
                    <span class="develop-title-sub">기업부설연구소 보유</span>
                    <hr class="develop-hr" />
                </div>
            </div>
        </div>
        <div class="col-lg-6 ps-lg-4 pe-0">
            <section id="develop-slick" class="splide develop-slick">
                <div class="splide__track">
                    <ul class="splide__list">
                        <li class="splide__slide">
                            <div class="card border-0">
                                <img class="object-fit-cover"
                                    src="https://archivemotion.com/wp-content/uploads/2024/06/develop-1.png"
                                    loading="lazy" />
                            </div>
                        </li>
                        <li class="splide__slide">
                            <div class="card border-0">
                                <img class="object-fit-cover"
                                    src="https://archivemotion.com/wp-content/uploads/2024/06/develop-2.png"
                                    loading="lazy" />
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
        </div>
    </div>
</div>

<!-- //develop -->

<!-- fabric -->

<div class="fabric d-flex flex-column-reverse flex-lg-column container-fluid container-main">
    <div class="d-flex justify-content-end d-block d-lg-none">
        <div class="fabric-item border-top-0">
            <div class="fabric-item-title">COLOR</div>
            <div class="fabric-item-hover">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/color.png" alt="knit" loading="lazy">
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-end">
        <div class="fabric-item">
            <div class="fabric-item-title">WOVEN</div>
            <div class="fabric-item-hover">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/woven.png" alt="woven" loading="lazy">
            </div>
        </div>
        <div class="fabric-item">
            <div class="fabric-item-title">KNIT</div>
            <div class="fabric-item-hover">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/knit.png" alt="knit" loading="lazy">
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-between align-items-start">
        <div class="d-flex flex-column justify-content-between py-4 py-lg-5">
            <div class="fabric-title fw-bold">FABRIC</div>
            <div class="fabric-title-des py-2 py-lg-3">
                <div>레퍼런스</div>
                <div class="fw-bold">맞춤 원단 소재를</div>
                <div><span class="fw-bold">제작 및 개발해 </span>드립니다.</div>
            </div>
            <div class="fabric-title-sub">브랜드 감성에 맞는 단독 개발된 원단 소재를 제안드리고 독특한 터치감을 제공합니다.</div>
        </div>
        <div class="d-none d-lg-block">
            <div class="fabric-item border-top-0">
                <div class="fabric-item-title">COLOR</div>
                <div class="fabric-item-hover">
                    <img src="https://archivemotion.com/wp-content/uploads/2024/06/color.png" alt="knit" loading="lazy">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- //fabric -->

<!-- global -->

<div
    class="global container-fluid container-main text-white align-items-start align-content-lg-center position-relative background-img">
    <div class="global-title-top mb-2 mb-lg-3">GLOBAL MANUFACTURING SYSTEM</div>
    <div class="global-title-center">
        글로벌 생산체계 <br class="d-block d-lg-none">구축으로<br class="d-none d-lg-block">
        보다 빠르고 <br class="d-block d-lg-none">효율적인 시스템
    </div>
</div>
<div class="global-box">
    <section id="global-splide" class="splide">
        <div class="splide__track">
            <ul class="splide__list">
                <li class="splide__slide">
                    <div class="card border-0">
                        <img class="global-item w-100 h-100" loading="lazy"
                            src="https://archivemotion.com/wp-content/uploads/2024/06/global-1.png" />
                        <div class="card-body px-0 pt-4 pb-0">
                            <div class="global-item-title fw-bold">대한민국, 서울 (본사)</div>
                            <div class="global-item-sub">Seoul, Republic of Korea</div>
                        </div>
                    </div>
                </li>
                <li class="splide__slide">
                    <div class="card border-0">
                        <img class="global-item w-100 h-100" loading="lazy"
                            src="https://archivemotion.com/wp-content/uploads/2024/06/global-2.png" />
                        <div class="card-body px-0 pt-4 pb-0">
                            <div class="global-item-title fw-bold">대한민국, 대구 (생산 공장)</div>
                            <div class="global-item-sub">Daegu, Republic of Korea</div>
                        </div>
                    </div>
                </li>
                <li class="splide__slide">
                    <div class="card border-0">
                        <img class="global-item w-100 h-100" loading="lazy"
                            src="https://archivemotion.com/wp-content/uploads/2024/06/global-3.png" />
                        <div class="card-body px-0 pt-4 pb-0">
                            <div class="global-item-title fw-bold">중국, 베이징 (해외 지사)</div>
                            <div class="global-item-sub">Beijing, China</div>
                        </div>
                    </div>
                </li>
                <li class="splide__slide">
                    <div class="card border-0">
                        <img class="global-item w-100 h-100" loading="lazy"
                            src="https://archivemotion.com/wp-content/uploads/2024/06/global-4.png" />
                        <div class="card-body px-0 pt-4 pb-0">
                            <div class="global-item-title fw-bold">베트남, 다낭 (생산 공장)</div>
                            <div class="global-item-sub">Danang, Vietnam</div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </section>
</div>

<!-- //global -->

<!-- apparel -->

<div class="apparel container-fluid container-main">
    <div class="apparel-title fw-bold">APPAREL PROMOTION</div>
    <div class="apparel-title-sub fw-bold">국내/해외 직영 공장에서</div>
    <div class="apparel-title-sub">기획 상품 및 다품종 소량 생산을 지원합니다.</div>
    <div class="apparel-title-des mt-1 mt-lg-2">디자인 작업지시서, 샘플링, 패턴, 그레이딩, 생산(국내/중국/베트남) 등</div>
    <div class="apparel-title-des">의류 생산에 필요한 모든 단계를 지원하고 소량 및 대량 생산을 진행해 드립니다.</div>
    <div class="apparel-box d-flex flex-column flex-lg-row justify-content-between gap-1 gap-lg-5 w-100 flex-nowrap">
        <div for="c1" class="card">
            <div class="card__row"></div>
        </div>

        <div for="c2" class="card">
            <div class="card__row"></div>
        </div>

        <div for="c3" class="card">
            <div class="card__row"></div>
        </div>

        <div for="c4" class="card">
            <div class="card__row"></div>
        </div>

        <div for="c5" class="card">
            <div class="card__row"></div>
        </div>
    </div>
</div>

<!-- //apparel -->

<!-- interview -->

<div id="interview" class="interview container-fluid container-main">
    <div class="interview-title fw-bold">Client Interview</div>
    <div class="interview-title-sub"><span class="fw-bold">검증된 브랜드</span>들이</div>
    <div class="interview-title-sub">아카이브모션을</div>
    <div class="interview-title-sub mb-3 mb-lg-5">선택한 이유</div>
    <div class="interview-grid container-main px-0">
        <div class="interview-1 row position-relative">
            <div class="col-5 col-lg-9">
                <img class="js-slidein w-100 h-100 object-fit-cover"
                    src="https://archivemotion.com/wp-content/uploads/2024/06/interview-1.png" loading="lazy">
                <div class="js-slidein interview-des-1 bg-white text-black w-50 pt-4 px-5 d-none d-lg-block">
                    <img src="https://archivemotion.com/wp-content/uploads/2024/06/interview-logo-1.png" loading="lazy">
                    <div>
                        <img class="interview-quote"
                            src="https://archivemotion.com/wp-content/uploads/2024/06/quote.png" loading="lazy">
                    </div>
                    <div class="interview-content">
                        아카이브 모션의 감각적인 패브릭은<br>
                        한 끗 차이로 달라지는 섬세한 디자인 작업의 <br>
                        완성도를 높여주는 중요한 요소이며<br>
                        높은 만족도를 제공해 줍니다.
                    </div>
                    <div class="interview-action d-flex gap-3 mt-1">
                        <span class="fw-bold">하크어패럴 siyazu</span><span>디자인팀 박빛나 실장님</span>
                    </div>
                </div>
            </div>
            <div class="js-slidein col-7 col-lg-3 position-relative">
                <div class="d-block d-lg-none">
                    <div class="d-flex justify-content-end">
                        <img class="interview-logo position-absolute z-2"
                            src="https://archivemotion.com/wp-content/uploads/2024/06/interview-logo-1.png"
                            loading="lazy">
                    </div>
                    <div>
                        <img class="interview-quote"
                            src="https://archivemotion.com/wp-content/uploads/2024/06/quote.png" loading="lazy">
                    </div>
                    <div class="interview-content">아카이브 모션의 <br class="d-block d-lg-none">감각적인 패브릭은 한 끗 차이로
                        <br class="d-block d-lg-none">달라지는 섬세한 디자인 작업의 <br class="d-block d-lg-none">
                        완성도를 높여주는 <br class="d-block d-lg-none">
                        중요한 요소이며 <br class="d-block d-lg-none">
                        높은 만족도를 제공해 줍니다.
                    </div>
                    <div class="interview-action d-flex flex-column flex-lg-row gap-1 gap-lg-3 mt-2 mt-lg-0">
                        <span class="fw-bold">하크어패럴 siyazu</span><span>디자인팀 박빛나 실장님</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="js-slidein interview-2 d-flex flex-column">
            <img class="interview-img-logo interview-logo d-none d-lg-block"
                src="https://archivemotion.com/wp-content/uploads/2024/06/interview-logo-2.png" loading="lazy">
            <div class="row">
                <div class="interview-box col-7 col-lg-12">
                    <div class="d-flex justify-content-end">
                        <img class="interview-img-logo interview-img-logo-lg interview-logo d-block d-lg-none"
                            src="https://archivemotion.com/wp-content/uploads/2024/06/interview-logo-2.png"
                            loading="lazy">
                    </div>
                    <div>
                        <img class="interview-quote"
                            src="https://archivemotion.com/wp-content/uploads/2024/06/quote.png" loading="lazy">
                    </div>
                    <div class="interview-content">아카이브모션의 <br class="d-block d-lg-none">개발 원단이 <br>4세대 디자이너 <br
                            class="d-block d-lg-none">브랜드의 감성까지<br>만족시킬 수 있다는 것이 <br class="d-block d-lg-none">놀랍습니다!
                    </div>
                    <div class="interview-action interview-zone d-flex gap-1 gap-lg-3">
                        <span class="fw-bold">의류 협회</span>
                        <span>서동찬 디자이너</span>
                    </div>
                </div>
                <div class="js-slidein col-5 col-lg-12">
                    <img class="w-100 h-100 object-fit-cover"
                        src="https://archivemotion.com/wp-content/uploads/2024/06/interview-2.png" loading="lazy">
                </div>
            </div>
        </div>
        <div class="js-slidein interview-3 row flex-row-reverse flex-lg-row position-relative py-4">
            <div class="col-7 col-lg-5">
                <div class="interview-des-3 bg-white text-black">
                    <img class="interview-logo position-absolute"
                        src="https://archivemotion.com/wp-content/uploads/2024/06/interview-logo-3.png" loading="lazy">
                    <div>
                        <img class="interview-quote"
                            src="https://archivemotion.com/wp-content/uploads/2024/06/quote.png" loading="lazy">
                    </div>
                    <div class="interview-content w-100">
                        아카이브모션은 <br class="d-block d-lg-none">해외 소재에서 느껴지는 <br class="d-none d-lg-block">고유의 <br
                            class="d-block d-lg-none">고밀도 터치감의
                        퀄리트를 <br class="d-block d-lg-none">현물로 <br class="d-none d-lg-block">쓸 수 있어서 <br
                            class="d-block d-lg-none">근접 기획하기 너무 좋아요
                    </div>
                    <div class="interview-action interview-zone d-flex gap-1 gap-lg-3">
                        <span class="fw-bold">쿠어 디자인팀</span> <span>유철영 실장님</span>
                    </div>
                </div>
            </div>
            <div class="js-slidein col-5 col-lg-7">
                <img class="interview-banner-3 object-fit-cover"
                    src="https://archivemotion.com/wp-content/uploads/2024/06/interview-3.png" loading="lazy">
            </div>
        </div>
        <div class="js-slidein interview-5 d-none d-lg-block"></div>
        <div class="js-slidein interview-4 row flex-row-reverse flex-lg-row">
            <div class="col-5 col-lg-8">
                <img class="w-100 h-100 object-fit-cover"
                    src="https://archivemotion.com/wp-content/uploads/2024/06/interview-4.png" loading="lazy">
            </div>
            <div class="interview-des-4 position-relative col-7 col-lg-4 d-flex justify-content-between flex-column">
                <img class="interview-logo"
                    src="https://archivemotion.com/wp-content/uploads/2024/06/interview-logo-4.png" loading="lazy">
                <div class="bg-white text-black">
                    <div class="mt-3 mt-lg-0">
                        <img class="interview-quote"
                            src="https://archivemotion.com/wp-content/uploads/2024/06/quote.png" loading="lazy">
                    </div>
                    <div class="interview-content">
                        퀄리티를 생명으로 하는만큼<br class="d-block d-lg-none"> 믿고 맏길 수 있는<br>
                        유일한 원단 협력사입니다.<br class="d-block d-lg-none"> 항상 트렌드에 맞는<br>
                        다양한 원단을 좋은 퀄리티에<br class="d-block d-lg-none"> 제공해 주셔서<br class="d-none d-lg-block">
                        만족합니다.
                    </div>
                    <div class="interview-action interview-zone d-flex flex-column flex-lg-row gap-1 gap-lg-3">
                        <span class="fw-bold">이스트엔드</span> <span>박일호 그룹장님</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- /interview -->

<!-- clients -->

<div id="clients" class="clients container-fluid container-main">
    <div class="clients-title fw-bold">Clients List</div>
    <div class="clients-box d-none d-lg-block">
        <div class="clients-items">
            <?php
            for ($row = 1; $row <= 5; $row++): ?>
                <div class="row gap-2 gap-lg-5">
                    <?php
                    for ($col = 1; $col <= 12; $col++):
                        $i = ($row - 1) * 12 + $col;
                        ?>
                        <div class="d-flex justify-content-center align-items-center">
                            <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-<?php echo $i; ?>.png"
                                alt="client-<?php echo $i; ?>" loading="lazy">
                        </div>
                        <?php
                    endfor; ?>
                </div>
                <?php
            endfor; ?>
        </div>
    </div>
    <div class="clients-box d-block d-lg-none">
        <div class="clients-items">
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-1.png" alt="client-1"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-13.png" alt="client-13"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-25.png" alt="client-25"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-37.png" alt="client-37"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-49.png" alt="client-49"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-2.png" alt="client-2"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-14.png" alt="client-14"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-26.png" alt="client-26"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-38.png" alt="client-38"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-50.png" alt="client-50"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-3.png" alt="client-3"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-15.png" alt="client-15"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-27.png" alt="client-27"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-39.png" alt="client-39"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-51.png" alt="client-51"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-4.png" alt="client-4"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-16.png" alt="client-16"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-28.png" alt="client-28"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-40.png" alt="client-40"
                    loading="lazy">
            </div>
            <!-- 20 -->
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-52.png" alt="client-52"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-5.png" alt="client-5"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-17.png" alt="client-17"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-29.png" alt="client-29"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-41.png" alt="client-41"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-53.png" alt="client-53"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-6.png" alt="client-6"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-18.png" alt="client-18"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-30.png" alt="client-30"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-42.png" alt="client-42"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-54.png" alt="client-54"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-7.png" alt="client-7"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-19.png" alt="client-19"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-31.png" alt="client-31"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-43.png" alt="client-43"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-55.png" alt="client-55"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-8.png" alt="client-8"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-20.png" alt="client-20"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-32.png" alt="client-32"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-44.png" alt="client-44"
                    loading="lazy">
            </div>
            <!-- 40 -->
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-56.png" alt="client-56"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-9.png" alt="client-9"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-21.png" alt="client-21"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-33.png" alt="client-33"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-45.png" alt="client-45"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-57.png" alt="client-57"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-10.png" alt="client-10"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-22.png" alt="client-22"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-34.png" alt="client-34"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-46.png" alt="client-46"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-58.png" alt="client-58"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-11.png" alt="client-11"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-23.png" alt="client-23"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-35.png" alt="client-35"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-47.png" alt="client-47"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-59.png" alt="client-59"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-12.png" alt="client-12"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-24.png" alt="client-24"
                    loading="lazy">
            </div>

            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-36.png" alt="client-36"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-48.png" alt="client-48"
                    loading="lazy">
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <img src="https://archivemotion.com/wp-content/uploads/2024/06/client-60.png" alt="client-60"
                    loading="lazy">
            </div>
        </div>
    </div>
</div>

<!-- clients -->

<!-- partners -->

<div class="partners container-fluid container-main">
    <div class="partners-title fw-bold">Partners</div>
    <div class="d-flex align-items-start flex-wrap partners-box">
        <div class="partners-items">
            <div class="partners-sub fw-bold">MOU</div>
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-1.svg" alt="partners-1"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-2.svg" alt="partners-2"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-3.svg" alt="partners-3"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-4.svg" alt="partners-4"
                loading="lazy">
        </div>
        <div class="partners-items">
            <div class="partners-sub fw-bold">INSPECTION</div>
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-5.svg" alt="partners-5"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-6.svg" alt="partners-6"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-7.svg" alt="partners-7"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-8.svg" alt="partners-8"
                loading="lazy">
        </div>
        <div class="partners-items">
            <div class="partners-sub fw-bold">TREND</div>
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-9.svg" alt="partners-9"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-10.svg" alt="partners-10"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-11.svg" alt="partners-11"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-12.svg" alt="partners-12"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-13.svg" alt="partners-13"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-14.svg" alt="partners-14"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-15.svg" alt="partners-15"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-16.svg" alt="partners-16"
                loading="lazy">
        </div>
        <div class="partners-items">
            <div class="partners-sub fw-bold">CERTIFICATION</div>
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-17.svg" alt="partners-17"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-18.svg" alt="partners-18"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-19.svg" alt="partners-19"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-20.svg" alt="partners-20"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-21.svg" alt="partners-21"
                loading="lazy">
        </div>
        <div class="partners-items">
            <div class="partners-sub fw-bold">ESG</div>
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-22.svg" alt="partners-22"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-23.svg" alt="partners-22"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-24.svg" alt="partners-22"
                loading="lazy">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/partners/partners-25.svg" alt="partners-27"
                loading="lazy">
        </div>
    </div>
</div>

<!-- //partners -->

<!-- faq -->

<div id="faq" class="faq container-fluid container-main bg-black">
    <div class="faq-title fw-bold text-white">FAQ</div>
    <div class="accordion accordion-flush" id="faq">
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-heading-1">
                <button class="accordion-button collapsed gap-2 align-items-center py-4" type="button"
                    data-bs-toggle="collapse" data-bs-target="#flush-collapse-1" aria-expanded="false"
                    aria-controls="flush-collapse-1">
                    <div class="faq-sub fw-bold pe-2 pe-lg-3">Q</div>
                    <div class="faq-a">원단 개발 요청 시 평균 소요되는 기간 및 비용은 어떻게 되나요?</div>
                </button>
            </h2>
            <div id="flush-collapse-1" class="accordion-collapse collapse" aria-labelledby="flush-heading-1">
                <div class="accordion-body d-flex gap-2">
                    <div class="faq-sub fw-bold pe-2 pe-lg-3 invisible">Q</div>
                    <div class="faq-a">원단 개발 비용은 소재에 따라 다르며 약 100~200만원의 시직 비용이 발생 됩니다. 개발 기간은 평균 30~50일 소요 됩니다.
                    </div>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-heading-2">
                <button class="accordion-button collapsed gap-2 align-items-center py-4" type="button"
                    data-bs-toggle="collapse" data-bs-target="#flush-collapse-2" aria-expanded="false"
                    aria-controls="flush-collapse-2">
                    <div class="faq-sub fw-bold pe-2 pe-lg-3">Q</div>
                    <div class="faq-a">시즌 상담은 원하는 날짜에 가능한가요?</div>
                </button>
            </h2>
            <div id="flush-collapse-2" class="accordion-collapse collapse" aria-labelledby="flush-heading-2">
                <div class="accordion-body d-flex gap-2">
                    <div class="faq-sub fw-bold pe-2 pe-lg-3 invisible">Q</div>
                    <div class="faq-a">네, 시즌 상담은 내방/방문 모두 무료로 가능합니다.</div>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-heading-3">
                <button class="accordion-button collapsed gap-2 align-items-center py-4" type="button"
                    data-bs-toggle="collapse" data-bs-target="#flush-collapse-3" aria-expanded="false"
                    aria-controls="flush-collapse-3">
                    <div class="faq-sub fw-bold pe-2 pe-lg-3">Q</div>
                    <div class="faq-a">원단 품목별 스와치북 또는 퀄리티를 먼저 받아볼 수 있나요?</div>
                </button>
            </h2>
            <div id="flush-collapse-3" class="accordion-collapse collapse" aria-labelledby="flush-heading-3">
                <div class="accordion-body d-flex gap-2">
                    <div class="faq-sub fw-bold pe-2 pe-lg-3 invisible">Q</div>
                    <div class="faq-a">
                        거래 고객사의 경우 요청 시 언제든지 저희 직원이 1~2일 내 사무실로 전달 드립니다. 다만, 신규 고객사의 경우 상담 전 퀄리티
                        발송은 지원하지 않고 있습니다.
                    </div>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-heading-4">
                <button class="accordion-button collapsed gap-2 align-items-center py-4" type="button"
                    data-bs-toggle="collapse" data-bs-target="#flush-collapse-4" aria-expanded="false"
                    aria-controls="flush-collapse-4">
                    <div class="faq-sub fw-bold pe-2 pe-lg-3">Q</div>
                    <div class="faq-a">샘플 요청 시 비용이 발생되나요?</div>
                </button>
            </h2>
            <div id="flush-collapse-4" class="accordion-collapse collapse" aria-labelledby="flush-heading-4">
                <div class="accordion-body d-flex gap-2">
                    <div class="faq-sub fw-bold pe-2 pe-lg-3 invisible">Q</div>
                    <div class="faq-a">메인 오더 진행 시 샘플 비용은 부과되지 않으며 만약 오더 진행이 되지 않는 경우 별도 청구 됩니다.</div>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-heading-5">
                <button class="accordion-button collapsed gap-2 align-items-center py-4" type="button"
                    data-bs-toggle="collapse" data-bs-target="#flush-collapse-5" aria-expanded="false"
                    aria-controls="flush-collapse-5">
                    <div class="faq-sub fw-bold pe-2 pe-lg-3">Q</div>
                    <div class="faq-a">의류 생산 요청 시 해외에서 소량 생산도 가능하나요?</div>
                </button>
            </h2>
            <div id="flush-collapse-5" class="accordion-collapse collapse" aria-labelledby="flush-heading-5">
                <div class="accordion-body d-flex gap-2">
                    <div class="faq-sub fw-bold pe-2 pe-lg-3 invisible">Q</div>
                    <div class="faq-a">네, 국내/해외(중국,베트남) 생산 모두 가능합니다.</div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- //faq -->

<!-- contact form -->

<div id="contact" class="contact-form container-fluid container-main text-white">
    <div class="contact-form-title fw-bold mb-3 mb-lg-5">무료 상담신청</div>
    <div class="d-flex justify-content-center">
        <form action="#" class="w-100" style="max-width: 480px">
            <div class="contact-label text-end fw-bold"><i class="fa-solid fa-circle align-middle"
                    style="color: #ff0000; font-size: 0.25rem"></i> 필수</div>
            <div class="mb-3">
                <label for="company" class="contact-label">회사명 <i class="fa-solid fa-circle align-middle"
                        style="color: #ff0000; font-size: 0.25rem"></i></label>
                <input type="text" class="form-control" id="company" />
            </div>
            <div class="mb-3">
                <label for="brand" class="contact-label">브랜드명 <i class="fa-solid fa-circle align-middle"
                        style="color: #ff0000; font-size: 0.25rem"></i></label>
                <input type="text" class="form-control" id="brand" />
            </div>
            <div class="mb-3">
                <label for="brandHome" class="contact-label">브랜드 홈페이지 </label>
                <input type="text" class="form-control" id="brandHome" />
            </div>
            <div class="d-flex gap-3 mb-3">
                <div class="w-50">
                    <label for="representative" class="contact-label">담당자 <i class="fa-solid fa-circle align-middle"
                            style="color: #ff0000; font-size: 0.25rem"></i></label>
                    <input type="text" class="form-control" id="representative" />
                </div>
                <div class="w-50">
                    <label for="position" class="contact-label">직책 또는 직급</label>
                    <input type="text" class="form-control" id="position" />
                </div>
            </div>
            <div class="d-flex gap-3 mb-3">
                <div class="w-50">
                    <label for="contact" class="contact-label">연락처 <i class="fa-solid fa-circle align-middle"
                            style="color: #ff0000; font-size: 0.25rem"></i></label>
                    <input type="tel" class="form-control" id="contact" />
                </div>
                <div class="w-50">
                    <label for="email" class="contact-label">이메일</label>
                    <input type="email" class="form-control" id="email" />
                </div>
            </div>
            <div class="mb-5">
                <label class="contact-label mb-2">카테고리</label>
                <div class="d-flex flex-wrap gap-2">
                    <div>
                        <input type="radio" class="contact-label btn-check" name="options" id="option1" />
                        <label class="btn btn-dark" for="option1">시즌상담</label>
                    </div>
                    <div>
                        <input type="radio" class="contact-label btn-check" name="options" id="option2" />
                        <label class="btn btn-dark" for="option2">원단수배</label>
                    </div>
                    <div>
                        <input type="radio" class="contact-label btn-check" name="options" id="option3" />
                        <label class="btn btn-dark" for="option3">원단개발</label>
                    </div>
                    <div>
                        <input type="radio" class="contact-label btn-check" name="options" id="option4" />
                        <label class="btn btn-dark" for="option4">의류생산</label>
                    </div>
                    <div>
                        <input type="radio" class="contact-label btn-check" name="options" id="option5" />
                        <label class="btn btn-dark" for="option5">기타</label>
                    </div>
                </div>
            </div>

            <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-light">신청하기</button>
            </div>
        </form>
    </div>
</div>
<!-- //contact form -->

<!-- //container -->

<?php get_footer(); ?>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        var lastScrollTop = 0, delta = 15;

        window.addEventListener("scroll", function () {
            var header = document.querySelector("header");
            var headerHeight = header.offsetHeight;
            var st = window.scrollY;

            if (Math.abs(lastScrollTop - st) <= delta) {
                return;
            }
            if ((st > lastScrollTop) && (lastScrollTop > 0)) {
                header.style.top = `-${headerHeight}px`;
            } else {
                header.style.top = "0px";
            }
            lastScrollTop = st;

            document.querySelectorAll(".js-slidein").forEach(function (element) {
                var elementTop = element.getBoundingClientRect().top + window.scrollY;
                var winTop = window.scrollY;
                var winHeight = window.innerHeight;
                if (elementTop < winTop + winHeight - 100) {
                    element.classList.add("js-slidein-visible");
                }
            });
        });
    });

    // develop slick
    document.addEventListener("DOMContentLoaded", function () {
        new Splide("#develop-slick", {
            type: "loop",
            perPage: 1,
            perMove: 1,
            arrows: false,
            pagination: false,
            width: "100%",
            padding: { right: "5em" },
            speed: 500,
            gap: "1.25rem",
            autoplay: true,
            easing: 'cubic-bezier(0.25, 1, 0.5, 1)',
            mediaQuery: 'min',
            wheel: true,
            breakpoints: {
                576: {
                    perPage: 1,
                    gap: "1.5rem",
                    padding: { right: "10em" },
                },
                1200: {
                    perPage: 1,
                    gap: "2rem",
                    padding: { right: "10rem" },
                },
                1400: {
                    perPage: 1,
                    gap: "2.875rem",
                    padding: { right: "20em" },
                }
            }
        }).mount();
    });

    // global splide
    document.addEventListener("DOMContentLoaded", function () {
        new Splide("#global-splide", {
            type: "loop",
            perPage: 1,
            perMove: 1,
            arrows: false,
            pagination: true,
            width: "100%",
            padding: { right: "5em" },
            speed: 500,
            gap: "1.25rem",
            easing: 'cubic-bezier(0.25, 1, 0.5, 1)',
            mediaQuery: 'min',
            wheel: true,
            breakpoints: {
                576: {
                    perPage: 2,
                    gap: "1.5em",
                    padding: { right: "6em" },
                    pagination: false,
                },
                1200: {
                    perPage: 3,
                    gap: "2.875rem",
                    padding: { right: "10em" },
                    pagination: false,
                }
            }
        }).mount();
    });

    // splide count
    document.addEventListener("DOMContentLoaded", function () {
        new Splide("#splide-count", {
            type: "loop",
            slidesToShow: 3,
            perPage: 3,
            centerMode: true,
            arrows: false,
            pagination: false,
            width: "100%",
            gap: 8,
            easing: 'cubic-bezier(0.25, 1, 0.5, 1)',
            mediaQuery: 'min',
            wheel: true,
            breakpoints: {
                576: {
                    gap: 16,
                    pagination: false,
                },
            }
        }).mount();
    });

    document.addEventListener("DOMContentLoaded", () => {
        const counters = document.querySelectorAll(".counter");
        let countersStarted = false;

        const isElementInViewport = (el) => {
            const rect = el.getBoundingClientRect();
            return (
                rect.top >= 0 &&
                rect.left >= 0 &&
                rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                rect.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
        };

        const startCounters = () => {
            if (!countersStarted) {
                countersStarted = true;
                counters.forEach((counter) => {
                    counter.innerText = "0";
                    const updateCounter = () => {
                        const target = +counter.getAttribute("data-target").replace(/,/g, "");
                        const count = +counter.innerText.replace(/,/g, "");

                        const increment = target / (target < 10 ? 10 : 200);

                        if (count < target) {
                            counter.innerText = new Intl.NumberFormat('en-US', { maximumSignificantDigits: 3 }).format(Math.ceil(count + increment));
                            setTimeout(updateCounter, 5);
                        } else {
                            counter.innerText = new Intl.NumberFormat('en-US', { maximumSignificantDigits: 3 }).format(target);
                            setTimeout(updateCounter, 5);
                        }
                    };
                    updateCounter();
                });
            }
        };

        const handleScroll = () => {
            if (counters.length > 0 && isElementInViewport(counters[0])) {
                startCounters();
                window.removeEventListener("scroll", handleScroll); // Remove the scroll event after starting the counters
            }
        };

        window.addEventListener("scroll", handleScroll);
        handleScroll(); // Check immediately if the element is in the viewport
    });
</script>